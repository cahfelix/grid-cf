
module.exports = function( grunt ) {
    /*======================================================================
    // Variaveis
    /*=====================================================================*/
    var config = {
        assetsPath: 'assets'
    }

    grunt.initConfig({
        // gets the package vars
        pkg: grunt.file.readJSON('package.json'),

        // setting folder templates
        dirs: {
            assetsPath: 'assets',
            css:        'assets/css',
            js:         'assets/js',
            sass:       'assets/sass',
            img:        'assets/img',
            fonts:      'assets/fonts',
            core:       'core',
            tmp:        'tmp'
        },  
        sass: {
            dist: {
              files: {
                '<%= dirs.css %>/application.css': '<%= dirs.sass %>/application.scss',
                // 'path/to/output.css': 'path/to/input.scss'
              }
            }
        },  
        cssmin: {
            minify: {
                expand: true,
                cwd:    '<%= dirs.css %>',
                src:    ['*.css', '!*.min.css'],
                dest:   '<%= dirs.css %>',
                ext:    '.min.css'
            }
        },        
        watch: {
            sass: {
                files: ['<%= dirs.sass %>/**/*.scss'],
                tasks: ['sass', 'cssmin']
            },
            options: {
                liverload: true,
            }           
        },
    });

    /*=======================================================================
    // Plugins
    /*=====================================================================*/
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    

    /*=======================================================================
    // Tarefas
    /*=====================================================================*/
    grunt.registerTask( 'default', [
        'sass',
        'cssmin',
        'watch'
    ]);

    grunt.registerTask( 'w', [ 'watch' ] );

};