# Grid CF #

Algumas vezes preferimos utilizar um sistema de grid feito por nós e não algum framework do mercado como o Bootstrap ou o Foundation. Isso tem suas grandes vantagens.

Este codigo permite que usemos um grid-system de uma maneira mais enxuta e eficiente. A função é voltada para projetos mais simples. A mesma pode receber como parâmetros um ou mais valores para as colunas


## Observações ##
1. Se necessário, execute os comandos com sudo
2. Os comandos devem ser executados na mesma pasta onde se encontra esse arquivo
 
## Para configurar o Grunt ##
1. Instale o nodejs através do link: [NodeJS via Package Manager](https://github.com/joyent/node/wiki/installing-node.js-via-package-manager)
2. Instale o Sass na sua máquina: `$ gem install sass`
3. Instale o Grunt na sua máquina: `$ npm install -g grunt grunt-cli`
4. Instale o Grunt no projeto: `$ npm install grunt --save-dev`
5. Instale as dependências do Grunt: `$ npm install`

## Comandos para rodar tasks ##
 *   EXECUTA TODAS AS TASKS:   grunt